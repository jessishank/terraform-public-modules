# Create Basic VM

Will create a folder with the `folder_name` variable if it doesn't already exist and create a VM in that folder. 

see [variables.tf](./variables.tf) for defaults and descriptions. 

Current only output is the ip address of the machine. 


Example Usage: 

```
module "create_vm" {
  source = "bitbucket.org/jessishank/terraform-public-modules//basic_vm"
  password = "vsphere password"
  name = "covfefe-vm"
}
```

Will Create one vm in the `vsca.level11.local` vSphere and name it covfefe-vm. You will be able to access the ip by `module.create_vm.ip_address`.