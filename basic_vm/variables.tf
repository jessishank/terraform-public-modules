variable "username" {
  default = "administrator@vcenter-sso.local"
}

variable "password" {}

variable "root_passwd" {
  description = "Password of root account of provisioned vm. highly recommended to change this.."
  default = "iamroot"
}

variable "name" {
  description = "name of VM"
}

variable "vsphere_server" {
  default = "vcsa.level11.local"
}

variable "vm_template" {
  default = "ubuntu-1604-docker-base-medium"
}

variable "datacenter" {
  default = "Level11-Datacenter"
}

variable "datastore" {
  default = "datastore_worf"
}

variable "resource_pool" {
  default = "prod-eng"
}

variable "network" {
  default = "VM Network"
}

variable "folder_name" {
  default = "terraform_vms"
  description = "vSphere folder to put provisioned VM in"
}

variable "num_cpu" {
  default = "2"
  description = "Number of virtual CPUs"
}

variable "memory" {
  default = "4096"
  description = "Memory to give machine (bytes)"
}

variable "root_size" {
  default = "60"
  description = "The size of the root disk"
}

variable "node_count" {
  default = 1
  description = "number of nodes to create."
}

variable "thin_provision" {
  default = "true"
  description = "Allocate disk space when used"
}
