# Configure the VMware vSphere Provider
provider "vsphere" {
  user           = "${var.username}"
  password       = "${var.password}" 
  vsphere_server = "${var.vsphere_server}"
  version = "~> 1.0.0"
  # if you have a self-signed cert
  allow_unverified_ssl = true
}

data "vsphere_datacenter" "dc" {
  name = "${var.datacenter}"
}

data "vsphere_datastore" "datastore" {
  name = "${var.datastore}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_resource_pool" "pool" {
  name = "${var.resource_pool}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_network" "network" {
  name = "${var.network}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_virtual_machine" "template" {
  name = "${var.vm_template}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

# FIXME: This was commented out since we would error out if the folder already created
## Create a folder
#resource "vsphere_folder" "create" {
#  datacenter_id = "${data.vsphere_datacenter.dc.id}"
#  type = "vm"
#  path = "${var.folder_name}"
#}

resource "vsphere_virtual_machine" "web" {
  count  = "${var.node_count}"
  name   = "${var.node_count == "1" ? var.name : format("%s-%d", var.name, count.index)}"

  resource_pool_id = "${data.vsphere_resource_pool.pool.id}"
  datastore_id = "${data.vsphere_datastore.datastore.id}"

  num_cpus  = "${var.num_cpu}"
  memory = "${var.memory}"
  guest_id = "${data.vsphere_virtual_machine.template.guest_id}"
  scsi_type = "${data.vsphere_virtual_machine.template.scsi_type}"

  folder = "${var.folder_name}"

  network_interface {
    network_id = "${data.vsphere_network.network.id}"
    adapter_type = "${data.vsphere_virtual_machine.template.network_interface_types[0]}"
  }

  disk {
    name = "${var.node_count == "1" ? var.name : format("%s-%d", var.name, count.index)}.vmdk"
    size = "${var.root_size}"
    thin_provisioned = "${var.thin_provision}"
  }

  clone {
    template_uuid = "${data.vsphere_virtual_machine.template.id}"
    customize {
      linux_options {
        host_name = "${var.node_count == "1" ? var.name : format("%s-%d", var.name, count.index)}"
        domain = "level11.local"
      }
      network_interface {}
    }
  }
}
