output "ip_address" {
  value = "${vsphere_virtual_machine.web.*.network_interface.0.device_address}"
}
