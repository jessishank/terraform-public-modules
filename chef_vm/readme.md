# Create basic vm w/ chef automation

There are four variables that you will need to set, either on the command line or with a `terraform.tfvars` file. I recommend tfvars so you don't have to worry about it again.

 - `vsphere_password`: pasword to admin account of vsphere 
 - `chef_key_pem_filepath`: Full filepath to the pem file of your chef user account. 
 - `ssh_key_fp`: Full filepath to the ssh key of the VM. Likely called `pe_id_rsa`, and you will use it to log in to the box later.
 - `chef_key_username`: your chef username. 