# Configure the VMware vSphere Provider
provider "vsphere" {
  user           = "${var.username}"
  password       = "${var.password}" 
  vsphere_server = "${var.vsphere_server}"
  version = "~> 1.0.0"
  # if you have a self-signed cert
  allow_unverified_ssl = true
}


# Create a folder
resource "vsphere_folder" "create" {
  datacenter = "${var.datacenter}"
  path = "${var.folder_name}"
}



resource "vsphere_virtual_machine" "web" {
  name   = "${var.name}"
  datacenter = "${var.datacenter}"
  folder = "${var.folder_name}"
  vcpu   = 2
  memory = 4096
  resource_pool = "${var.resource_pool}"

  network_interface {
    label = "VM Network"
  }

  disk {
    datastore = "${var.datastore}"
    template = "${var.vm_template}"
  }

  provisioner "file" {
    destination = "/etc/chef/encrypted_data_bag_secret"
    content = "${file(var.encrypted_data_bag_secret_fp)}"
  }

  provisioner "chef" {
    node_name  = "${var.name}"
    server_url = "https://manage.chef.io/organizations/l11"
    user_name  = "${var.chef_key_username}"
    user_key   = "${file(var.chef_key_pem_filepath)}"
    recreate_client  = true  # remove client of this name before re-registering
    
    attributes_json = <<-EOF
      {
        "chef_client": {
          "interval" : 60,
          "log_dir": "/var/log/chef/"
        }
      }
    EOF

    
    run_list   = "${var.chef_run_list}"
    connection {
      agent    = true
      user     = "leveler"
      private_key = "${file(var.ssh_key_fp)}"
    }
  }
}
