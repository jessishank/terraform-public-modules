variable "username" {
  description = "vsphere username"
  default = "administrator@vcenter-sso.local"
}

variable "password" {}

variable "ssh_key_fp" {
  description = "Filepath to ssh key for provisioned vm."
}

variable "name" {
  description = "name of VM"
}

variable "chef_key_username" {
  description = "chef username to log in with to register with server"
}

variable "chef_key_pem_filepath" {
  description = "fully qualified path to chef_key_username's pem file."
}

variable "encrypted_data_bag_secret_fp" {
  description = "Filepath for encrypted data_bag_secret if you want to use access encrypted data"
  default = "/dev/null"
}

variable "vsphere_server" {
  default = "vcsa.level11.local"
}

variable "vm_template" {
  default = "ubuntu-1604-docker-base-medium"
}

variable "resource_pool" {
  default = "ces.level11.local"
}

variable "datacenter" {
  default = "Level11-Datacenter"
}

variable "datastore" {
  default = "datastore_lambtron"
}

variable "folder_name" {
  default = "terraform_vms"
  description = "vSphere folder to put provisioned VM in"
}

variable "vcpu" {
  default = "2"
  description = "Number of virtual CPUs"
}

variable "memory" {
  default = "4096"
  description = "Memory to give machine (bytes)"
}

variable "chef_run_list" {
  default =  [ "chef-client::default", "chef-client::delete_validation", "internal_vm_automation"]  
  description = "Run list on startup of vm. Mostly will just need these 3 recipes."
}