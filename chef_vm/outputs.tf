output "ip_address" {
  value = "${vsphere_virtual_machine.web.network_interface.0.ipv4_address}"
}